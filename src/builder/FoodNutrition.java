package builder;

public class FoodNutrition {

  private final int id;
  private final String name;
  private final int calories;
  private final int servingSize;
  private final int fat;
  private final String description;


  private FoodNutrition(int id, String name, int calories, int servingSize, int fat,
      String description) {
    this.id = id;
    this.name = name;
    this.calories = calories;
    this.servingSize = servingSize;
    this.fat = fat;
    this.description = description;
  }

  public int getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public int getCalories() {
    return calories;
  }

  public int getServingSize() {
    return servingSize;
  }

  public int getFat() {
    return fat;
  }

  public String getDescription() {
    return description;
  }

  public static class Builder {

    private final int id;
    private final String name;
    private int calories;
    private int servingSize;
    private int fat;
    private String description;

    public Builder(int id, String name) {
      this.id = id;
      this.name = name;
    }

    public Builder calories(int calories) {
      this.calories = calories;
      return this;
    }

    public Builder servingSize(int servingSize) {
      this.servingSize = servingSize;
      return this;
    }

    public Builder fat(int fat) {
      this.fat = fat;
      return this;
    }

    public Builder description(String description) {
      this.description = description;
      return this;
    }

    public FoodNutrition build() {
      return new FoodNutrition(id, name, calories, servingSize, fat, description);
    }
  }
}